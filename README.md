# A demo app for "Microservice Ruby complete stack: grape + activerecord + swagger&rack"

See the article on Roialty Blog, [The Grape Swagger stack](http://www.roialty.com/ruby-the-grape-swagger-stack/)

## The Grape Swagger stack, a Ruby minimal, clean and complete stack for microservices

### Intro
Microservices are now very common in various architectures and environments.
The Ruby word is traditionally bound to the microservice approach by the glorious minimal web framework Sinatra.
There are many good ruby micro frameworks but Grape coupled with Swagger UI via the grape-swagger gem really shines as a fast, clean and complete solution.
With Grape + Swagger UI we can build the Restful API service and have a good auto-generated GUI to test and debug the API.

### A microservice example with Grape and Swagger
As example in this article we’ll build a simple microservice that exposes a Restful CRUD resource, notes.
The Note model will contain: author, title, body, summary and the timestamps. The API will expose the classic CRUD actions: create, read, update, delete and the list of the resources available.
